/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package hikari2014.amakuni.org;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int app_background=0x7f040000;
        public static final int dark_red=0x7f040002;
        public static final int light_red=0x7f040001;
        public static final int tab_indicator_text=0x7f040005;
        public static final int text_tab_selected=0x7f040003;
        public static final int text_tab_unselected=0x7f040004;
    }
    public static final class dimen {
        public static final int corner_radius=0x7f050000;
        public static final int tab_space=0x7f050002;
        public static final int tab_space_plus1=0x7f050003;
        public static final int tab_space_top=0x7f050001;
    }
    public static final class drawable {
        public static final int activitatsmenu=0x7f020000;
        public static final int amakuni_fonsblanc=0x7f020001;
        public static final int amakunimenu=0x7f020002;
        public static final int celda_cabecera=0x7f020003;
        public static final int celda_cuerpo=0x7f020004;
        public static final int comercial=0x7f020005;
        public static final int didactic=0x7f020006;
        public static final int divulgatiu=0x7f020007;
        public static final int escenari_selected=0x7f020008;
        public static final int escenari_unselected=0x7f020009;
        public static final int estands_selected=0x7f02000a;
        public static final int estands_unselected=0x7f02000b;
        public static final int estandsmenu=0x7f02000c;
        public static final int gastronomia=0x7f02000d;
        public static final int hikari_bar=0x7f02000e;
        public static final int hikari_stage=0x7f02000f;
        public static final int hikari_wc=0x7f020010;
        public static final int hikaribar=0x7f020011;
        public static final int hikaristage=0x7f020012;
        public static final int hikariwc=0x7f020013;
        public static final int ic_launcher=0x7f020014;
        public static final int ic_tab_arroba_selected=0x7f020015;
        public static final int ic_tab_arroba_unselected=0x7f020016;
        public static final int ic_tab_sharp_selected=0x7f020017;
        public static final int ic_tab_sharp_unselected=0x7f020018;
        public static final int icona=0x7f020019;
        public static final int informatiu=0x7f02001a;
        public static final int interior_selected=0x7f02001b;
        public static final int interior_unselected=0x7f02001c;
        public static final int logohikari=0x7f02001d;
        public static final int mapa=0x7f02001e;
        public static final int mapamenu=0x7f02001f;
        public static final int oci=0x7f020020;
        public static final int tab_arroba=0x7f020021;
        public static final int tab_escenari=0x7f020022;
        public static final int tab_estands=0x7f020023;
        public static final int tab_focus=0x7f020024;
        public static final int tab_indicator=0x7f020025;
        public static final int tab_interior=0x7f020026;
        public static final int tab_selected=0x7f020027;
        public static final int tab_sharp=0x7f020028;
        public static final int tab_unselected=0x7f020029;
        public static final int twittermenu=0x7f02002a;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f080006;
        public static final int Valoracio=0x7f080046;
        public static final int cabezerahorario=0x7f08000a;
        public static final int comentari=0x7f080048;
        public static final int editText1=0x7f08004b;
        public static final int icon=0x7f080040;
        public static final int imageStand=0x7f08003e;
        public static final int imageView1=0x7f080007;
        public static final int iniLayout=0x7f08003a;
        public static final int item_detail_container=0x7f080000;
        public static final int item_list=0x7f080001;
        public static final int mapaEvent=0x7f08003b;
        public static final int menuIcon=0x7f080002;
        public static final int progressBar1=0x7f08004a;
        public static final int scroll=0x7f08003d;
        public static final int scrollhorariesc=0x7f080008;
        public static final int scrollhorariint=0x7f080037;
        public static final int scrollhoraristand=0x7f080039;
        public static final int sendButton=0x7f080049;
        public static final int starvot=0x7f080047;
        public static final int tabla_cuerpo=0x7f080009;
        public static final int tableRow1=0x7f08000f;
        public static final int tableRow2=0x7f080014;
        public static final int tableRow3=0x7f080019;
        public static final int tableRow4=0x7f08001e;
        public static final int tableRow5=0x7f080023;
        public static final int tableRow6=0x7f080028;
        public static final int tableRow7=0x7f08002d;
        public static final int tableRow8=0x7f080032;
        public static final int tableRow9=0x7f080038;
        public static final int textDurada=0x7f080005;
        public static final int textRowMenu=0x7f08003c;
        public static final int textRowTitol=0x7f080003;
        public static final int textSituacio=0x7f080004;
        public static final int textStand=0x7f08003f;
        public static final int textView1=0x7f08000b;
        public static final int textView10=0x7f080016;
        public static final int textView11=0x7f080017;
        public static final int textView12=0x7f080018;
        public static final int textView13=0x7f08001a;
        public static final int textView14=0x7f08001b;
        public static final int textView15=0x7f08001c;
        public static final int textView16=0x7f08001d;
        public static final int textView17=0x7f08001f;
        public static final int textView18=0x7f080020;
        public static final int textView19=0x7f080021;
        public static final int textView2=0x7f08000c;
        public static final int textView20=0x7f080022;
        public static final int textView21=0x7f080024;
        public static final int textView22=0x7f080025;
        public static final int textView23=0x7f080026;
        public static final int textView24=0x7f080027;
        public static final int textView25=0x7f080029;
        public static final int textView26=0x7f08002a;
        public static final int textView27=0x7f08002b;
        public static final int textView28=0x7f08002c;
        public static final int textView29=0x7f08002e;
        public static final int textView3=0x7f08000d;
        public static final int textView30=0x7f08002f;
        public static final int textView31=0x7f080030;
        public static final int textView32=0x7f080031;
        public static final int textView33=0x7f080033;
        public static final int textView34=0x7f080034;
        public static final int textView35=0x7f080035;
        public static final int textView36=0x7f080036;
        public static final int textView4=0x7f08000e;
        public static final int textView5=0x7f080010;
        public static final int textView6=0x7f080011;
        public static final int textView7=0x7f080012;
        public static final int textView8=0x7f080013;
        public static final int textView9=0x7f080015;
        public static final int title=0x7f080041;
        public static final int tweetAutor=0x7f080043;
        public static final int tweetAvatar=0x7f080042;
        public static final int tweetCont=0x7f080045;
        public static final int tweetData=0x7f080044;
    }
    public static final class layout {
        public static final int activity_item_detail=0x7f030000;
        /** 
    Layout alias to replace the single-pane version of the layout with a
    two-pane version on Large screens.

    For more on layout aliases, see:
    http://developer.android.com/training/multiscreen/screensizes.html#TaskUseAliasFilters
    

    Layout alias to replace the single-pane version of the layout with a
    two-pane version on Large screens.

    For more on layout aliases, see:
    http://developer.android.com/training/multiscreen/screensizes.html#TaskUseAliasFilters
    
         */
        public static final int activity_item_list=0x7f030001;
        public static final int activity_item_twopane=0x7f030002;
        public static final int colaboradorfila=0x7f030003;
        public static final int colaboradorllista=0x7f030004;
        public static final int headermenu=0x7f030005;
        public static final int horariesc=0x7f030006;
        public static final int horariint=0x7f030007;
        public static final int horaristand=0x7f030008;
        public static final int main=0x7f030009;
        public static final int mapa=0x7f03000a;
        public static final int menu=0x7f03000b;
        public static final int menurow=0x7f03000c;
        public static final int standview=0x7f03000d;
        public static final int tab_indicator=0x7f03000e;
        public static final int tabnewsfeed=0x7f03000f;
        public static final int tabsched=0x7f030010;
        public static final int tweet=0x7f030011;
        public static final int votacio=0x7f030012;
    }
    public static final class string {
        public static final int About=0x7f060008;
        public static final int AboutDesc=0x7f06000d;
        public static final int Activitat=0x7f06005c;
        public static final int Amakuni=0x7f060037;
        public static final int Aula=0x7f060064;
        public static final int BonOdori=0x7f060075;
        public static final int Bonsai=0x7f060067;
        public static final int BudoShin=0x7f060071;
        public static final int Carrega=0x7f060078;
        public static final int Collaborator=0x7f060006;
        public static final int CollaboratorDesc=0x7f06000b;
        public static final int ConferenciaEventosJap=0x7f06006c;
        public static final int Contes=0x7f06006d;
        public static final int DemostracioIkebana=0x7f06006b;
        public static final int DemostracioSushi=0x7f060069;
        public static final int EnviaOp=0x7f06007b;
        public static final int Escenari=0x7f060060;
        public static final int Estand15=0x7f06005f;
        public static final int Estand8=0x7f06005e;
        public static final int Estands=0x7f060062;
        public static final int Final=0x7f06005b;
        public static final int Hora11=0x7f060038;
        public static final int Hora12=0x7f060039;
        public static final int Hora1230=0x7f06003a;
        public static final int Hora13=0x7f06003b;
        public static final int Hora1330=0x7f06003c;
        public static final int Hora14=0x7f06003d;
        public static final int Hora1430=0x7f06003e;
        public static final int Hora1445=0x7f06003f;
        public static final int Hora15=0x7f060040;
        public static final int Hora1530=0x7f060041;
        public static final int Hora1545=0x7f060042;
        public static final int Hora16=0x7f060043;
        public static final int Hora1630=0x7f060044;
        public static final int Hora17=0x7f060045;
        public static final int Hora1715=0x7f060046;
        public static final int Hora1730=0x7f060047;
        public static final int Hora1745=0x7f060048;
        public static final int Hora18=0x7f060049;
        public static final int Hora1830=0x7f06004a;
        public static final int Hora19=0x7f06004b;
        public static final int Hora1915=0x7f06004c;
        public static final int Hora1930=0x7f06004d;
        public static final int Hora1945=0x7f06004e;
        public static final int Hora20=0x7f06004f;
        public static final int Hora2015=0x7f060050;
        public static final int Hora2030=0x7f060051;
        public static final int Hora2045=0x7f060052;
        public static final int Hora21=0x7f060053;
        public static final int Hora2130=0x7f060054;
        public static final int Hora22=0x7f060055;
        public static final int Hora2215=0x7f060056;
        public static final int Hora2230=0x7f060057;
        public static final int Hora2245=0x7f060058;
        public static final int Hora23=0x7f060059;
        public static final int Inici=0x7f06005a;
        public static final int Interior=0x7f060061;
        public static final int Japones=0x7f060068;
        public static final int Judo=0x7f06006e;
        public static final int Karate=0x7f060074;
        public static final int KimonoShow=0x7f060070;
        public static final int Kokeshi=0x7f060073;
        public static final int Lloc=0x7f06005d;
        public static final int Map=0x7f060004;
        public static final int MapDesc=0x7f060009;
        public static final int Newsfeed=0x7f060007;
        public static final int NewsfeedDesc=0x7f06000c;
        public static final int Origami=0x7f06006f;
        public static final int Placa=0x7f060077;
        public static final int Polivalent=0x7f060063;
        public static final int Schedule=0x7f060005;
        public static final int ScheduleDesc=0x7f06000a;
        public static final int Sorteig=0x7f060076;
        public static final int Stand1=0x7f06000e;
        public static final int Stand10=0x7f060018;
        public static final int Stand11=0x7f060019;
        public static final int Stand12=0x7f06001a;
        public static final int Stand13=0x7f06001b;
        public static final int Stand14=0x7f06001c;
        public static final int Stand15=0x7f06001d;
        public static final int Stand16=0x7f06001e;
        public static final int Stand1Text=0x7f060036;
        public static final int Stand1bis=0x7f06000f;
        public static final int Stand2=0x7f060010;
        public static final int Stand3=0x7f060011;
        public static final int Stand4=0x7f060012;
        public static final int Stand5=0x7f060013;
        public static final int Stand6=0x7f060014;
        public static final int Stand7=0x7f060015;
        public static final int Stand8=0x7f060016;
        public static final int Stand9=0x7f060017;
        public static final int StandDurada=0x7f06001f;
        public static final int StandDuradaDesDe16Fins1930=0x7f060021;
        public static final int StandDuradaDesDe18=0x7f060022;
        public static final int StandDuradaFins16=0x7f060020;
        public static final int StandSituacio1=0x7f060023;
        public static final int StandSituacio10=0x7f06002c;
        public static final int StandSituacio11=0x7f06002d;
        public static final int StandSituacio12=0x7f06002e;
        public static final int StandSituacio13=0x7f06002f;
        public static final int StandSituacio14=0x7f060030;
        public static final int StandSituacio15=0x7f060031;
        public static final int StandSituacio16=0x7f060032;
        public static final int StandSituacio17=0x7f060033;
        public static final int StandSituacio18=0x7f060034;
        public static final int StandSituacio2=0x7f060024;
        public static final int StandSituacio3=0x7f060025;
        public static final int StandSituacio4=0x7f060026;
        public static final int StandSituacio5=0x7f060027;
        public static final int StandSituacio6=0x7f060028;
        public static final int StandSituacio7=0x7f060029;
        public static final int StandSituacio8=0x7f06002a;
        public static final int StandSituacio9=0x7f06002b;
        public static final int StandSituacioSala=0x7f060035;
        public static final int TallerJocsTrad=0x7f06006a;
        public static final int TallerKimono=0x7f060065;
        public static final int TallerShodo=0x7f060066;
        public static final int TallerShogi=0x7f060072;
        public static final int TevaOpinio=0x7f06007a;
        public static final int Valoracio=0x7f060079;
        public static final int app_name=0x7f060003;
        public static final int arroba=0x7f060000;
        public static final int hello=0x7f060002;
        public static final int hello_world=0x7f06007f;
        public static final int sharp=0x7f060001;
        public static final int title_activity_vote=0x7f06007e;
        public static final int title_item_detail=0x7f06007d;
        public static final int title_item_list=0x7f06007c;
    }
    public static final class style {
        public static final int LightTabWidget=0x7f070000;
        public static final int MyLightTheme=0x7f070001;
        public static final int MyLightTheme_NoShadow=0x7f070002;
    }
}
