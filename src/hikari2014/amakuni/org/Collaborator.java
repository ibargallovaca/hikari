package hikari2014.amakuni.org;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Collaborator extends Activity{
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.standview);
        Bundle extras = getIntent().getExtras();
        TextView text = (TextView) findViewById(R.id.textStand);
        ImageView imatge = (ImageView) findViewById(R.id.imageStand);
        text.setText(extras.getCharSequence("textStand"));
        imatge.setImageResource(extras.getInt("imatgeStand"));
        
        
    }

}
