package hikari2014.amakuni.org;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class CollaboratorActivity extends ListActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.colaboradorllista);
		CollaboratorFila colmenu_data[] = new CollaboratorFila[] {
				new CollaboratorFila(R.drawable.informatiu, getResources()
						.getString(R.string.Stand1), getResources().getString(
						R.string.StandSituacio1), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.divulgatiu, getResources()
						.getString(R.string.Stand1bis), getResources()
						.getString(R.string.StandSituacio1), getResources()
						.getString(R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand2), getResources().getString(
						R.string.StandSituacio2), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand3), getResources().getString(
						R.string.StandSituacio3), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand4), getResources().getString(
						R.string.StandSituacio4), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand5), getResources().getString(
						R.string.StandSituacio5), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.gastronomia, getResources()
						.getString(R.string.Stand6), getResources().getString(
						R.string.StandSituacio6), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.oci, getResources().getString(
						R.string.Stand7), getResources().getString(
						R.string.StandSituacio7), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand8), getResources().getString(
						R.string.StandSituacio8), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand9), getResources().getString(
						R.string.StandSituacio9), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.gastronomia, getResources()
						.getString(R.string.Stand10), getResources().getString(
						R.string.StandSituacio10), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.divulgatiu, getResources()
						.getString(R.string.Stand11), getResources().getString(
						R.string.StandSituacio11), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.comercial, getResources()
						.getString(R.string.Stand12), getResources().getString(
						R.string.StandSituacio12), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.gastronomia, getResources()
						.getString(R.string.Stand13), getResources().getString(
						R.string.StandSituacio13), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.gastronomia, getResources()
						.getString(R.string.Stand14), getResources().getString(
						R.string.StandSituacio14), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.gastronomia, getResources()
						.getString(R.string.Stand15), getResources().getString(
						R.string.StandSituacio15), getResources().getString(
						R.string.StandDurada)),
				new CollaboratorFila(R.drawable.didactic, getResources()
						.getString(R.string.Stand16), getResources().getString(
						R.string.StandSituacio16), getResources().getString(
						R.string.StandDurada)) };
		CollaboratorAdapter adapter = new CollaboratorAdapter(this,
				R.layout.colaboradorfila, colmenu_data);
		setListAdapter(adapter);

	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intentOpen;
		switch (position) {
		default:
			intentOpen = new Intent(this, VoteActivity.class);
			intentOpen.putExtra("standPos", position);
			startActivity(intentOpen);
			break;
		/*
		 * case 0: intentOpen = new Intent(this,Collaborator.class);
		 * intentOpen.putExtra("imatgeStand", R.drawable.ic_menu_cards);
		 * intentOpen.putExtra("textStand",
		 * getResources().getString(R.string.Stand1Text));
		 * startActivity(intentOpen); break; case 1: intentOpen = new
		 * Intent(this,CollaboratorActivity.class); startActivity(intentOpen);
		 * break; case 2: intentOpen = new Intent(this,SchedulleActivity.class);
		 * startActivity(intentOpen); break; case 3: intentOpen = new
		 * Intent(this,NewsfeedActivity.class); startActivity(intentOpen);
		 * break; case 4: intentOpen = new Intent(this,AboutActivity.class);
		 * startActivity(intentOpen); break;
		 */
		}

	}
}