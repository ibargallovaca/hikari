package hikari2014.amakuni.org;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CollaboratorAdapter extends ArrayAdapter<CollaboratorFila>{
	Context context;
    int layoutResourceId;   
    CollaboratorFila data[] = null;
    
   
    public CollaboratorAdapter(Context context, int layoutResourceId, CollaboratorFila[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CollaboratorHolder holder = null;
       
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
           
            holder = new CollaboratorHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.menuIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.textRowTitol);
            holder.txtSituacio = (TextView)row.findViewById(R.id.textSituacio);
            holder.txtDurada = (TextView)row.findViewById(R.id.textDurada);
            
           
            row.setTag(holder);
        }
        else
        {
            holder = (CollaboratorHolder)row.getTag();
        }
       
        CollaboratorFila colfila = data[position];
        holder.txtTitle.setText(colfila.title);
        holder.txtSituacio.setText(colfila.situacio);
        holder.txtDurada.setText(colfila.durada);
        holder.imgIcon.setImageResource(colfila.icon);
       
        return row;
    }
   
    static class CollaboratorHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtSituacio;
        TextView txtDurada;
    }

}
