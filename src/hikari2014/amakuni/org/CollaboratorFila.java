package hikari2014.amakuni.org;

public class CollaboratorFila {
    public int icon;
    public String title;
    public String situacio;
    public String durada;
    
    public CollaboratorFila(){
        super();
    }
   
    public CollaboratorFila(int icon, String title, String situacio, String durada) {
        super();
        this.icon = icon;
        this.title = title;
        this.situacio = situacio;
        this.durada = durada;
    }
}