package hikari2014.amakuni.org;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;


public class HikariActivity extends Activity implements OnTouchListener{
	CountDownTimer comptador;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final View iniLayout = findViewById(R.id.iniLayout);
        iniLayout.setOnTouchListener(this);
        comptador = new CountDownTimer(4000, 4000) {

            public void onTick(long millisUntilFinished) {
                
            }

            public void onFinish() {
                changeActivity();
            }
         }.start();

        
        
    }


    public void changeActivity(){
    	Intent intentOpen = new Intent(this,MenuActivity.class);
		startActivity(intentOpen);
		comptador.cancel();
		this.finish();    	
    }
    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		changeActivity();
		return false;
	}

	
}