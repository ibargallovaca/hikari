package hikari2014.amakuni.org;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class MenuActivity extends ListActivity  {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		ListView listView = getListView();
		RowMenu rowmenu_data[] = new RowMenu[] {
				new RowMenu(R.drawable.mapamenu, getResources().getString(
						R.string.Map), getResources().getString(
						R.string.MapDesc)),
				new RowMenu(R.drawable.estandsmenu, getResources().getString(
						R.string.Collaborator), getResources().getString(
						R.string.CollaboratorDesc)),
				new RowMenu(R.drawable.activitatsmenu, getResources()
						.getString(R.string.Schedule), getResources()
						.getString(R.string.ScheduleDesc)),
				new RowMenu(R.drawable.twittermenu, getResources().getString(
						R.string.Newsfeed), getResources().getString(
						R.string.NewsfeedDesc)),
				new RowMenu(R.drawable.amakunimenu, getResources().getString(
						R.string.About), getResources().getString(
						R.string.AboutDesc)) };
		RowMenuAdapter adapter = new RowMenuAdapter(this, R.layout.menurow,
				rowmenu_data);
		View header = (View)getLayoutInflater().inflate(R.layout.headermenu, null);
		listView.addHeaderView(header);
		listView.setAdapter(adapter);
		
		

	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intentOpen;
		switch (position) {
		case 1:
			intentOpen = new Intent(this, MapActivity.class);
			startActivity(intentOpen);
			break;
		case 2:
			intentOpen = new Intent(this, CollaboratorActivity.class);
			startActivity(intentOpen);
			break;
		case 3:
			intentOpen = new Intent(this, SchedulleActivity.class);
			startActivity(intentOpen);
			break;
		case 4:
			intentOpen = new Intent(this, NewsfeedTab.class);
			startActivity(intentOpen);
			break;
		case 5:
			intentOpen = new Intent(this, Collaborator.class);
			intentOpen.putExtra("imatgeStand", R.drawable.amakuni_fonsblanc);
			intentOpen.putExtra("textStand",
					getResources().getString(R.string.Amakuni));
			startActivity(intentOpen);
			break;
		}

	}

	
}