package hikari2014.amakuni.org;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsfeedActivity extends ListActivity {
	private ArrayList<Tweet> tweets = new ArrayList<Tweet>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new MyTask().execute();
	}

	private class MyTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progressDialog;

		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(NewsfeedActivity.this, "",
					getResources().getString(R.string.Carrega), true);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				TwitterWrapper twitterWrapper = new TwitterWrapper();
				Bundle extras = getIntent().getExtras();
				// HttpClient hc = new DefaultHttpClient();
				// HttpGet get;
				if (extras.getBoolean("timeline")) {
					setTweets(twitterWrapper.getTimeLine());
					// get = new HttpGet(
					// "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=amakuni_reus&include_rts=true&count=20");
					// "https://api.twitter.com/1.1/search/tweets.json?q=%23freebandnames&since_id=24012619984051000&max_id=250126199840518145&result_type=mixed&count=4");
				} else {
					setTweets(twitterWrapper.getTwitterHashtags("#hikarireus"));
					// get = new HttpGet(
					// "http://search.twitter.com/search.json?rpp=50&q=hikarireus&result_type=mixed");
					// "https://api.twitter.com/1.1/search/tweets.json?q=%23freebandnames&since_id=24012619984051000&max_id=250126199840518145&result_type=mixed&count=4");
				}

				// http://search.twitter.com/search.json?rpp=50&q=hikarireus&result_type=mixed
				// http://api.twitter.com/1/statuses/user_timeline.json?screen_name=amakuni_reus&include_rts=true&count=20
				// HttpResponse rp = hc.execute(get);
				//
				// if (rp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// //String result = EntityUtils.toString(rp.getEntity());
				// if(extras.getBoolean("timeline")){
				// twitterTimeLine(twitterWrapper.getTimeLine());
				// //twitterTimeLine(result);
				// }else{
				// //twitterHashTags(result);
				// }
				//
				// }
			} catch (Exception e) {
				Log.e("TwitterFeedActivity", "Error loading JSON", e);
			}
			return null;
		}

		private void setTweets(List<twitter4j.Status> timeLine) {
			Tweet tweet;
			for (twitter4j.Status status : timeLine) {
				tweet = new Tweet();
				tweet.autor = status.getUser().getName();
				tweet.contingut = status.getText();
				tweet.avatar = status.getUser().getProfileImageURL();
				java.text.DateFormat dateInstance = SimpleDateFormat
						.getDateInstance();
				tweet.temps = dateInstance.format(status.getCreatedAt());
				tweets.add(tweet);
			}

		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			setListAdapter(new TweetListAdapter(NewsfeedActivity.this,
					R.layout.tweet, tweets));
		}

	}

	private class TweetListAdapter extends ArrayAdapter<Tweet> {
		Context context;
		int layoutResourceId;
		ArrayList<Tweet> data = null;
		public ImageManager imageManager;

		public TweetListAdapter(Context context, int layoutResourceId,
				ArrayList<Tweet> data) {
			super(context, layoutResourceId, data);
			this.layoutResourceId = layoutResourceId;
			this.context = context;
			this.data = data;
			imageManager = new ImageManager(context.getApplicationContext(),
					36000000);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			TweetHolder holder = null;

			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(layoutResourceId, parent, false);

				holder = new TweetHolder();
				holder.tweetAvatar = (ImageView) row
						.findViewById(R.id.tweetAvatar);
				holder.tweetAutor = (TextView) row
						.findViewById(R.id.tweetAutor);
				holder.tweetCont = (TextView) row.findViewById(R.id.tweetCont);
				holder.tweetData = (TextView) row.findViewById(R.id.tweetData);

				row.setTag(holder);
			} else {
				holder = (TweetHolder) row.getTag();
			}

			Tweet colfila = data.get(position);
			if (colfila != null) {
				holder.tweetAutor.setText(colfila.autor);
				holder.tweetCont.setText(colfila.contingut);
				holder.tweetData.setText(colfila.temps);
				holder.tweetAvatar.setTag(colfila.avatar);
				imageManager.displayImage(colfila.avatar, holder.tweetAvatar,
						R.drawable.ic_launcher);
				// holder.tweetAvatar.setImageBitmap(getBitmap(colfila.avatar));
			}

			return row;
		}

	}

	static class TweetHolder {
		ImageView tweetAvatar;
		TextView tweetAutor;
		TextView tweetCont;
		TextView tweetData;
	}
}
