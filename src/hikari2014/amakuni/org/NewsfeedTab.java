package hikari2014.amakuni.org;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

public class NewsfeedTab extends TabActivity{
    /** Called when the activity is first created. */
	TabHost tabHost;
    @Override
    public void onCreate(Bundle savedInstanceState) {    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabnewsfeed);
        //boolean timeline = false;
        //RelativeLayout rel = (RelativeLayout) findViewById(id)
        
        
       
        tabHost = getTabHost();  // The activity TabHost
        Intent intent;  // Reusable Intent for each tab
        tabHost.setBackgroundColor(Color.WHITE);
        // Tab @
        
        
        intent = null;
     
        addTab("@", getResources().getString(R.string.arroba), R.drawable.tab_arroba, intent, true);
        // Tab mentions
        
        addTab("#",getResources().getString(R.string.sharp), R.drawable.tab_sharp, intent, false);
        /*tabHost.setOnTabChangedListener(this);	
		
        tabHost.getTabWidget().getChildAt(0)
		.setBackgroundColor(Color.WHITE);
		tabHost.getTabWidget().getChildAt(1)
		.setBackgroundColor(Color.rgb(240,240,240));
		*/

        tabHost.setCurrentTab(0);

    }

    private void addTab(String id, String label, int drawableId, Intent intent, Boolean adreca){
		TabHost.TabSpec spec; // Resusable TabSpec for each tab		
		spec = tabHost.newTabSpec(id);
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		intent = new Intent().setClass(this, NewsfeedActivity.class);
		intent.putExtra("timeline", adreca);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(label);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		tabHost.addTab(spec);
	}
    

    
    
}