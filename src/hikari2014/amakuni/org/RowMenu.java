package hikari2014.amakuni.org;

public class RowMenu {
    public int icon;
    public String title;
    public String desc;
    public RowMenu(){
        super();
    }
   
    public RowMenu(int icon, String title, String desc) {
        super();
        this.icon = icon;
        this.title = title;
        this.desc = desc;
    }
}