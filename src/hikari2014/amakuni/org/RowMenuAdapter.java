package hikari2014.amakuni.org;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RowMenuAdapter extends ArrayAdapter<RowMenu>{
	Context context;
    int layoutResourceId;   
    RowMenu data[] = null;
    
   
    public RowMenuAdapter(Context context, int layoutResourceId, RowMenu[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RowMenuHolder holder = null;
       
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
           
            holder = new RowMenuHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.menuIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.textRowTitol);
            holder.txtDesc = (TextView)row.findViewById(R.id.textRowMenu);
           
            row.setTag(holder);
        }
        else
        {
            holder = (RowMenuHolder)row.getTag();
        }
       
        RowMenu rowmenu = data[position];
        holder.txtTitle.setText(rowmenu.title);
        holder.txtDesc.setText(rowmenu.desc);
        holder.imgIcon.setImageResource(rowmenu.icon);
       
        return row;
    }
   
    static class RowMenuHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDesc;
    }

}
