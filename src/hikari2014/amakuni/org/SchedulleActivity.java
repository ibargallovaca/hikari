package hikari2014.amakuni.org;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

public class SchedulleActivity extends TabActivity implements
		OnTabChangeListener {
	TabHost tabHost;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabsched);
		Intent 	intent = null;
		Resources res = getResources(); // Resource object to get Drawables
		tabHost = getTabHost(); // The activity TabHost
		tabHost.setBackgroundColor(Color.WHITE);
		

		// Tab exterior
		
		
	//			.setIndicator(getResources().getString(R.string.Escenari), res.getDrawable(R.drawable.escenari))
	//			.setContent(intent);
		
		addTab("Exterior", getResources().getString(R.string.Escenari), R.drawable.tab_escenari, intent, R.layout.horariesc);
		

		// Tab sala polivalent
		addTab("Sala", getResources().getString(R.string.Interior), R.drawable.tab_interior, intent, R.layout.horariint);
		// Tab estands
		addTab("Estands", getResources().getString(R.string.Estands), R.drawable.tab_estands, intent, R.layout.horaristand);
		
		/*
		 * for(int i = 0; i < 3;i++){
		 * tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
		 * }
		 */
		/*tabHost.setOnTabChangedListener(this);

		tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.WHITE);
		
		tabHost.getTabWidget().getChildAt(1)
				.setBackgroundColor(Color.rgb(240, 240, 240));
		tabHost.getTabWidget().getChildAt(2)
		.setBackgroundColor(Color.rgb(240, 240, 240));*/

		tabHost.setCurrentTab(0);
	}

	private void addTab(String id, String label, int drawableId, Intent intent, int taula){
		TabHost.TabSpec spec; // Resusable TabSpec for each tab		
		spec = tabHost.newTabSpec(id);
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		intent = new Intent().setClass(this, SchedulleTable.class);
		intent.putExtra("taula", taula);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(label);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		tabHost.addTab(spec);
	}
	
	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		if ("Exterior".equals(tabId)) {
			tabHost.getTabWidget().getChildAt(0)
					.setBackgroundColor(Color.WHITE);
			tabHost.getTabWidget().getChildAt(1)
					.setBackgroundColor(Color.rgb(240, 240, 240));
			tabHost.getTabWidget().getChildAt(2)
			.setBackgroundColor(Color.rgb(240, 240, 240));
		}
		if ("Sala".equals(tabId)) {
			tabHost.getTabWidget().getChildAt(1)
					.setBackgroundColor(Color.WHITE);
			tabHost.getTabWidget().getChildAt(0)
					.setBackgroundColor(Color.rgb(240, 240, 240));
			tabHost.getTabWidget().getChildAt(2)
			.setBackgroundColor(Color.rgb(240, 240, 240));
		}
		if ("Estands".equals(tabId)) {
			tabHost.getTabWidget().getChildAt(2)
					.setBackgroundColor(Color.WHITE);
			tabHost.getTabWidget().getChildAt(0)
					.setBackgroundColor(Color.rgb(240, 240, 240));
			tabHost.getTabWidget().getChildAt(1)
			.setBackgroundColor(Color.rgb(240, 240, 240));
		}

	}
}