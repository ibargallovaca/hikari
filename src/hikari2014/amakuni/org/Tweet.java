package hikari2014.amakuni.org;

public class Tweet {
	public String avatar;
	public String autor;
	public String contingut;
	public String temps;
	
	public Tweet(){
		super();
	}
	
	public Tweet(String avatar, String autor, String contingut, String temps){
		this.avatar = avatar;
		this.autor = autor;
		this.contingut = contingut;
		this.temps = temps;
	}

}
