package hikari2014.amakuni.org;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

public class VoteActivity extends Activity {

	private ProgressBar progressBar;
	private EditText comentari;
	private RatingBar ratingBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.votacio);

		
		progressBar=(ProgressBar)findViewById(R.id.progressBar1);
		ratingBar = (RatingBar) findViewById(R.id.starvot);
		LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(Color.YELLOW,
				PorterDuff.Mode.SRC_ATOP);
		
		progressBar.setVisibility(View.GONE);
		
	}

	public void sendClick(View view) {
		Bundle extras = getIntent().getExtras();
		int posicio = extras.getInt("standPos");
		comentari=(EditText)findViewById(R.id.comentari);		
		new MyAsyncTask().execute(String.valueOf(posicio), comentari.getText().toString(), String.valueOf(ratingBar.getRating()));
	}
	
	
	private class MyAsyncTask extends AsyncTask<String, Integer, Double>{
		 
		@Override
		protected Double doInBackground(String... params) {
			sendData(params[0], params[1], params[2]);
			return null;
		}
 
		protected void onPostExecute(Double result){
			progressBar.setVisibility(View.GONE);
			Toast.makeText(getApplicationContext(), "command sent", Toast.LENGTH_LONG).show();
		}
		protected void onProgressUpdate(Integer... progress){
			progressBar.setProgress(progress[0]);
		}
 
		public void sendData(String posicio, String comentari, String rating) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://192.168.1.38/projects/hikari/votacions.php");
 
			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("posicio", posicio));
				nameValuePairs.add(new BasicNameValuePair("comentari", comentari));
				nameValuePairs.add(new BasicNameValuePair("rating", rating));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
 
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
 
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
 
	}
}
